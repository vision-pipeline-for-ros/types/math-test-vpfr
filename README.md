![alt text](https://gitlab.com/vision-pipeline-for-ros/vision-pipeline-for-ros/-/raw/master/icons/logo_long.svg)



# Vision Pipeline

[VPFR](https://gitlab.com/vision-pipeline-for-ros/vision-pipeline-for-ros)

[Wiki](https://gitlab.com/vision-pipeline-for-ros/vision-pipeline-for-ros/wikis/home)

---

# Math test VPFR
This is a type of vision pipeline.  
This type starts an instance of a vision pipeline.  
The instance then loads all algorithms that match the type.  
This type processes integer numbers and is used for testing.  


Details on this in the [wiki](https://gitlab.com/vision-pipeline-for-ros/types/math-test-vpfr/-/wikis/home)