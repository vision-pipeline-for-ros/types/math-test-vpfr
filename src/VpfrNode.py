#!/usr/bin/env python

"""
math-test-vpfr
Copyright 2020 Vision Pipeline for ROS
Author: Fabian Kranewitter <fabiankranewitter@gmail.com>
Author: Sebastian Bergner <sebastianbergner5892@gmail.com>
SPDX-License-Identifier: MIT
"""

import sys
from std_msgs.msg import Int64

from vpfr import *

if __name__ == "__main__":

    if len(sys.argv) < 4:
        print(
            "usage: VpfrNode.py subscribechannel publishchannel additionalArgs"
        )
    else:
        VisionPipeline(
            "vpfrmathtest", sys.argv[1], Int64, sys.argv[2], Int64, sys.argv
        )
